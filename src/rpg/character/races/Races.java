package rpg.character.races;

/**
 * This Enum provides a list of races to check easily the players or npcs race.
 *
 * @author Patrick Marx
 * @version 1.0
 */
public enum Races {
    DEMON,
    DWARF,
    ELF,
    GOBLIN,
    HUMAN,
    ORC
}
