package rpg.character;
import rpg.character.races.Races;
import rpg.util.Dice6;

import java.io.FileNotFoundException;

/**
 * @author Patrick Marx
 * @version 1.0
 */
public class PlayerCharacterCreator {
    PlayerCharacter player;

    private int intelligence;
    private int strength;
    private int constitution;
    private int charisma;
    private int wisdom;
    private int dexterity;
    private Races race;
    public PlayerCharacterCreator(String geschlecht, String pName, String race, String pClass){
        this.intelligence = rollAttributes();
        this.strength = rollAttributes();
        this.constitution = rollAttributes();
        this.charisma = rollAttributes();
        this.wisdom = rollAttributes();
        this.dexterity = rollAttributes();

        if(race.equalsIgnoreCase("Demon")){
            this.race = Races.DEMON;
        }else if (race.equalsIgnoreCase("Zwerg")){
            this.race = Races.DWARF;
        }else if (race.equalsIgnoreCase("Elf")){
            this.race = Races.ELF;
        }else if(race.equalsIgnoreCase("Goblin")){
            this.race = Races.GOBLIN;
        }else if(race.equalsIgnoreCase("Mensch")){
            this.race = Races.HUMAN;
        }else if(race.equalsIgnoreCase("Ork")){
            this.race = Races.ORC;
        }else{
            throw new IllegalArgumentException("Fehlerhafte Rasse angegeben");
        }

        player = new PlayerCharacter(geschlecht, pName, this.race, pClass, this.intelligence, this.constitution,
                this.charisma, this.strength, this.wisdom, this.dexterity);
    }


    public PlayerCharacter getPlayer(){
        return player;
    }
    /**
     *
     * @return The average of 4 rolls with a d6
     */
    private int rollAttributes(){
        int[] rolls = new int[4];
        Dice6 d6 = new Dice6();
        int avg = 0;
        for(int i = 0; i < rolls.length; i++){
            rolls[i] = d6.rollDice();
        }

        for(int i = 0; i < rolls.length; i++){
            avg = avg + rolls[i];
        }

        return avg;

    }
}
