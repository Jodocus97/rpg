package rpg.character;

import rpg.character.races.Races;

/**
 * This class is the base-class for the Characters of the game.
 *
 * @version 1.0
 */

public class Character {

    private String geschlecht;
    private String name;
    private Races rasse;
    private String classes;
    private int intelligence;
    private int constitution;
    private int charisma;
    private int strength;
    private int wisdom;
    private int dexterity;

    public Character(){

    }
    public Character(String geschlecht, String name, Races rasse, String classes, int intelligence, int constitution, int charisma, int strength, int wisdom, int dexterity){
        this.geschlecht = geschlecht;
        this.name = name;
        this.rasse = rasse;
        this.classes = classes;
        this.intelligence = intelligence;
        this.constitution = constitution;
        this.charisma = charisma;
        this.strength = strength;
        this.wisdom = wisdom;
        this.dexterity = dexterity;
    }

    /* Getter-Methoden */
    public String getGeschlecht(){
        return this.geschlecht;
    }

    public String getName(){
        return this.name;
    }

    public Races getRasse(){
        return this.rasse;
    }

    public String getClasses(){
        return this.classes;
    }

    public int getIntelligence(){
        return this.intelligence;
    }

    public int getConstitution(){
        return this.constitution;
    }

    public int getCharisma(){
        return this.charisma;
    }

    public int getStrength(){
        return this.strength;
    }

    public int getWisdom(){
        return this.wisdom;
    }

    public int getDexterity(){
        return this.dexterity;
    }

    /* Setter-Methoden */

    public void setGeschlecht(String geschlecht){
        this.geschlecht = geschlecht;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setRasse(Races race){
        this.rasse = race;
    }

    public void setClasses(String classes){
        this.classes = classes;
    }

    public void setIntelligence(int intelligence){
        this.intelligence = intelligence;
    }

    public void setConstitution(int constitution){
        this.constitution = constitution;
    }

    public void setCharisma(int charisma){
        this.charisma = charisma;
    }

    public void setStrength(int strength){
        this.strength = strength;
    }

    public void setWisdom(int wisdom){ this.wisdom = wisdom; }

    public void setDexterity(int dexterity){
        this.dexterity = dexterity;
    }

}
