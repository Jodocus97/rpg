package rpg.character;
import rpg.character.races.Races;
public class PlayerCharacter extends Character {
    public PlayerCharacter(String geschlecht, String name, Races race, String classes, int intelligence,
                           int constitution, int charisma, int strength, int wisdom, int dexterity){
        super(geschlecht, name, race, classes, intelligence, constitution, charisma, strength, wisdom, dexterity);
    }
}
