package rpg;
import java.util.Scanner;

import rpg.character.Character;
import rpg.character.PlayerCharacter;
import rpg.character.PlayerCharacterCreator;
import rpg.character.races.*;
import rpg.util.Dice6;
public class Charactersheet {

	    public static void main (String[] args){
	    	
	    	
	    	/*Also. Grob gesagt kann man sich merken, dass kurz nach der Charaktererstellung eine kleine Einfuehrugn ins Spiel kommen soll.
	    	 Was ja auch schon ein wenig Sinn ergibt, immerhin befinden wir uns in einem Rollenspiel... Also: Was funktioniert wie, was kann gemacht werden - und was nicht!
	    	 Die kleine Koboldhexe (sie bleibt vorerst noch namenlos) wird soweit alles erklaeren, wo man Ueberhaupt gelandet ist, was man hier
	    	 zu suchen hat, was man tun muss um in seine eigene Welt zurueckkehren zu koennen.
	    	 Die Welt wird kurz erklaert, die Umstaende werden eingefuehrt, usw. usf. ....*/
	    	

	        System.out.println("Du erwachst in einer kleinen, sehr bescheiden eingerichteten H�tte."+
	        					"\nDein Kopf tut weh und du bist sehr verwirrt, da du absolut keine Ahnung hast, wie du dorthin gelangt bist."+
	        					"\n\nEine kleine Gestalt mit einem spitzen Hut kniet neben dir und schaut dich sehr besorgt an."+
	        					"\nAls sie bemerkt, dass du die Augen aufgeschlagen hast, beginnt sie freudig auf dich einzureden:"+
	        					"\n\n'Willkommen Abenteurer!\nEs freut mich, dass du deinen Weg hierher gefunden hast!"+
	                            "\n...Wie war das? Du wei�t nicht mehr, wer du bist und hier her gekommen bist?"+
	                            "\nMir scheint, dass dies das Werk des Drachen von Corash war."+
	                            "\nDann m�ssen wir erst einmal schauen, wer du nun eigentlich bist. Erz�hle mir etwas �ber dich...' ");

	        String ageschlecht = "";
	        String bname = "";
	        String crasse = "";
	        String dklasse = "";
	        int eintelligenz = 0;
	        int fkonstitution = 0;
	        int gcharisma = 0;
	        int hstaerke = 0;
	        int iweisheit = 0;
	        int jgeschick = 0;
	        Scanner sc = new Scanner(System.in);

	        System.out.println("\n\nOkay, dann verrate mir zuerst, ob du m�nnlich oder weiblich bist!");
	        ageschlecht = sc.next();
	        
	        if (ageschlecht == "") {
	        	System.out.println("..nochmal von vorne. Bist du m�nnlich oder weiblich?");
	        	ageschlecht = sc.next();
	        }
	        
	        System.out.println("\n\nNun, du bist also "+ageschlecht+", dann verrate mir doch bitte noch deinen Namen!");
	        bname = sc.next();
	        
	        System.out.println("So, du hei�t also "+bname+". Erz�hle mir bitte, zu welcher Rasse du geh�rst."+
	        "\nBist du ein Mensch? Oder vielleicht doch ein ZWerg oder Goblin? Vielleicht auch ein Ork? Oder doch ein Elb?"+
	        		"\nOder gar ein D�mon?");
	        crasse = sc.next();
	        
	        System.out.println("\n\nGut, du bist also ein "+crasse+". Welcher Klasse geh�rst du an?"+
	        "\nHast du dich der Kunst der Magie verschrieben? Bist du ein Paladin oder Assassine?"+
	        		"\nGeh�rst du zu den Alchemisten, oder doch eher zu den Rittern?"+
	        "\nOder, ich wage es gar nicht zu sagen, bist du etwa ein Beschw�rer, oder gar ein Necromant?");
	        
	        dklasse = sc.next();
	        
	        System.out.println("\n\nGut. Dann wissen wir bereits, wer du �berhaupt bist, "+bname+"!\n"
	        		+ "Dann schauen wir doch mal, wie es um deinen Charakter bestellt ist!");
	        
	        

	        
	        
	        PlayerCharacterCreator spieler = new PlayerCharacterCreator(ageschlecht, bname, crasse, dklasse);
	        PlayerCharacter player = spieler.getPlayer();

			eintelligenz = player.getIntelligence();
			System.out.println("Deine Intelligenz liegt bei "+eintelligenz+"!");

			fkonstitution = player.getConstitution();
			System.out.println("Deine Konstitution liegt bei "+fkonstitution+"!");

			gcharisma = player.getCharisma();
			System.out.println("Dein Charisma liegt bei "+gcharisma+"!");

			hstaerke = player.getStrength();
			System.out.println("Deine St�rke liegt bei "+hstaerke+"!");

			iweisheit = player.getWisdom();
			System.out.println("Deine Weisheit liegt bei "+iweisheit+"!");

			jgeschick = player.getDexterity();
			System.out.println("Dein Geschick liegt bei "+jgeschick+"!");


	    }

	    /*
	    public static int werte (int ergebnis) {
	    	
	    	int wuerfel1 = 0;
	    	int wuerfel2 = 0;
	    	int wuerfel3 = 0;
	    	int wuerfel4 = 0;

	    	
	    	wuerfel1 = wuerfeln();
	    	wuerfel2 = wuerfeln();
	    	wuerfel3 = wuerfeln();
	    	wuerfel4 = wuerfeln();
	    	
	    	if (wuerfel1 <= wuerfel2 && wuerfel1 <= wuerfel3 && wuerfel1 <= wuerfel4) {
	    		wuerfel1 = 0;
	    	}
	    	if (wuerfel2 <= wuerfel1 && wuerfel2 <= wuerfel3 && wuerfel2 <= wuerfel4) {
	    		wuerfel2 = 0;
	    	}
	    	
	    	if (wuerfel3 <= wuerfel1 && wuerfel3 <= wuerfel2 && wuerfel3 <= wuerfel4) {
	    		wuerfel3 = 0;
	    	}
	    	if (wuerfel4 <= wuerfel1 && wuerfel4 <= wuerfel2 && wuerfel4 <= wuerfel3) {
	    		wuerfel4 = 0;
	    	}
	    	
	    	ergebnis = wuerfel1 + wuerfel2 + wuerfel3 + wuerfel4;
	    	
	    	return ergebnis;
	    }
	    
	    public static int wuerfeln () {
	    	
	    	Dice6 d6 = new Dice6();
	    	return d6.rollDice();
	    	
	    }

	    */
}
