package rpg.util;

public class Dice20 extends Dice {
    public Dice20(){
        super();
    }

    public int rollDice(){
        return rd.nextInt(21)+1;
    }
}
