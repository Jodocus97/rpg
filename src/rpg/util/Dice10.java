package rpg.util;

public class Dice10 extends Dice {
    public Dice10(){
        super();
    }

    public int rollDice(){
        return rd.nextInt(11)+1;
    }
}
