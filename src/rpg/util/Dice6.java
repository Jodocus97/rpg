package rpg.util;

public class Dice6 extends Dice {
    public Dice6(){
        super();
    }

    public int rollDice(){
        return rd.nextInt(7)+1;
    }
}
