package rpg.util;

public class Dice8 extends Dice {
    public Dice8(){
        super();
    }

    public int rollDice(){
        return rd.nextInt(9)+1;
    }
}
