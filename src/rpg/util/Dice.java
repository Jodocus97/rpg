package rpg.util;
import java.util.Random;

/**
 * This class provides a basic structure for a dice.
 * It is abstract, because in the game various kinds of dices appear (d6, d8, d10, d20 usw..)
 */
public abstract class Dice {
    Random rd;

    public Dice(){
        rd = new Random();
    }

    //This method rolls a dice
    public abstract int rollDice();
}
